# ElderHelperTexas Project Proposal

- Production Website Link: https://www.elderhelpertexas.me/
- API Website Link: https://cs373backend.elderhelpertexas.me/
- Postman Documentation: https://documenter.getpostman.com/view/17862950/2sA2r545M3
- Gitlab Pipeline Link: https://gitlab.com/ysahu.atx/cs373-group-23/-/pipelines
- GIT SHA: 467e278deca018b285fe65bc50534ed8cffab171


## Canvas / Slack group number: 23
## Team Members:
- Matthew Nguyen (EID: mtn988 , gitlab: NguyenMatthew)
- Yuvraj Sahu (EID: ys23486 , gitlab: ysahu.atx)
- Jose Monroy-Soto (EID: jms23633 , gitlab: josemonroysoto )
- Sidharth Srinivas (EID: ss97433, gitlab: kbsidsri)
- Adrian Gallardo (EID: amg8989 , gitlab: adrian.gallardo512) (Phase Leader - Organizes team with tasks and schedules meetings)

## Project Name: ElderHelperTexas.me
## Proposed Project:
- As the elderly population skyrockets across the country, senior citizens are increasingly unaware of the communities, services, and healthcare centers near their location. The proposed project is to design a website to help elderly people in the state of Texas find a place to live, services, and healthcare. This will help bridge that gap and increase the elderly population’s well-being in Texas.
## URLs of at least three data sources that you will programmatically scrape (at least one must be a RESTful API) (be very sure about this)
- [Google Maps](https://mapsplatform.google.com/)
- [American Hospital Directory](https://www.ahd.com/states/hospital_TX.html)
- [Eventbrite](https://www.eventbrite.com/d/united-states--texas/senior-events/)
## At least 3 models:
- Elderly Homes Locator
- Hospitals
- Events
## Estimated number of instances of each model:
- Elderly Homes: ~250
- Hospitals: 300+
- Services & Events: 200+
## Describe five of those attributes for each model that you can filter or sort
- **Elderly Homes**
    - Name
    - Address
    - Ratings
    - Website
    - Phone Number
    - Types (Assisted Living, Nursing Home, Independent Living)
- **Hospitals**
    - Name
    - City
    - Ownership (Non-profit, For-profit, Public)
    - County
    - Metro/Non-metro
    - License Type
- **Services & Events**
    - Name
    - Organization
    - Location
    - Date & Time
    - Price
## Instances of each model must connect to instances of at least two other models:
- Each instance of each type of model will be connected to the other models by proximity. 
## Describe two types of media for instances of each model
- **Elderly Homes**
    - Images
    - Map
    - Google Ratings/Maps
- **Hospitals**
    - Images
    - Videos
    - Text
- **Services & Events**
    - Images
    - Map
    - Text
## Describe three questions that your site will answer
- Where to find elderly homes?
- Where to find hospitals?
- What are events located?

## Estimated Completion Time (Per Member) Phase 4
- Matthew Nguyen 5
- Yuvraj Sahu 5
- Jose Monroy-Soto 5
- Sidharth Srinivas 7
- Adrian Gallardo 8

## Actual Completion Time (Per Member) Phase 4
- Matthew Nguyen 8
- Yuvraj Sahu 8
- Jose Monroy-Soto 6
- Sidharth Srinivas 8
- Adrian Gallardo 10

## Estimated Completion Time (Per Member) Phase 3
- Matthew Nguyen 5
- Yuvraj Sahu 8
- Jose Monroy-Soto 10
- Sidharth Srinivas 7
- Adrian Gallardo 14

## Actual Completion Time (Per Member) Phase 3
- Matthew Nguyen 8
- Yuvraj Sahu 11
- Jose Monroy-Soto 15
- Sidharth Srinivas 10
- Adrian Gallardo 18

## Estimated Completion Time (Per Member) Phase 2
- Matthew Nguyen 15
- Yuvraj Sahu 15
- Jose Monroy-Soto 15
- Sidharth Srinivas 15
- Adrian Gallardo 40

## Actual Completion Time (Per Member) Phase 2
- Matthew Nguyen 25
- Yuvraj Sahu 22
- Jose Monroy-Soto 18
- Sidharth Srinivas 25
- Adrian Gallardo 50

## Estimated Completion Time (Per Member) Phase 1
- Matthew Nguyen 10
- Yuvraj Sahu 9
- Jose Monroy-Soto 8
- Sidharth Srinivas 10
- Adrian Gallardo 16

## Actual Completion Time (Per Member) Phase 1
- Matthew Nguyen 14
- Yuvraj Sahu 12
- Jose Monroy-Soto 13
- Sidharth Srinivas 12
- Adrian Gallardo 14

## Comments
