// CardGridComponent.jsx

import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import HospitalCard from './HospitalCard';
import NursingHomeCard from './NursingHomeCard';
import SocialCard from './SocialCard';
import './CardGrid.css'

const CardGrid = ({ cardData, modelNum }) => {
    if (modelNum === 0) {
        return (
            <Row className="card-grid-container">
            {cardData.map((card, index) => (
                <Col key={index} sm={6} md={4} lg={3}>
                <HospitalCard {...card} />
                </Col>
            ))}
            </Row>
        );
    } else if(modelNum === 1) {
        return (
            <Row className="card-grid-container">
            {cardData.map((card, index) => (
                <Col key={index} sm={6} md={4} lg={3}>
                <NursingHomeCard {...card} />
                </Col>
            ))}
            </Row>
        );
    } else if(modelNum === 2) {
        return (
            <Row className="card-grid-container">
            {cardData.map((card, index) => (
                <Col key={index} sm={6} md={4} lg={3}>
                <SocialCard {...card} />
                </Col>
            ))}
            </Row>
        );
    }
};

export default CardGrid;
