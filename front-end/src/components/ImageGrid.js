import React from "react";
import ImageCard from "./ImageCard"; // Import your ImageCard component
import "./ImageGrid.css"; // Import your CSS file

const imageArray = [
  {
    src: "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/800px-Python-logo-notext.svg.png",
    alt: "Image 1",
  },
  {
    src: "https://www.google.com/url?sa=i&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FReact_%2528software%2529&psig=AOvVaw1GMfcotBG4l--QV37THKvh&ust=1707518501664000&source=images&cd=vfe&opi=89978449&ved=0CBMQjRxqFwoTCKD7sJ_onIQDFQAAAAAdAAAAABAE",
    alt: "Image 2",
  },
  {
    src: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Bootstrap_logo.svg/800px-Bootstrap_logo.svg.png",
    alt: "Image 3",
  },
  // Add more image data as needed
];

const ImageGrid = () => {
  return (
    <div className="image-grid">
      {imageArray.map((image, index) => (
        <ImageCard key={index} src={image.src} alt={image.alt} />
      ))}
    </div>
  );
};

export default ImageGrid;
