import { render, screen, cleanup } from "@testing-library/react"
import Home from "../pages/Home"
import About from "../pages/About"
import Hospitals from "../pages/Hospitals"
import Events from "../pages/ServicesAndEvents"
import ElderlyHomes from "../pages/ElderlyHomes"
import React from 'react';

describe('Test Front-End', () => {
    // Sample Test
    test('Sample Test', () => {
      // Perform the test using assertions
      expect(true).toBe(true);
    });
  
    // Test case 1: checks for title existence and content
    test('title existence and content', () => {
      render(<Home></Home>);
      const element = screen.getByTestId("home title");
      expect(element).toBeInTheDocument();
      expect(element).toHaveTextContent("Elder Helper Texas");
    });
    
    // Test case 2: checks for image existence
    test('image existence', () => {
      render(<Home></Home>);
      const imgElement  = screen.getByTestId("test2 image"); 
      expect(imgElement).toBeInTheDocument();
      expect(imgElement).toHaveAttribute('src');
    });

    //Test Case 3: checks for our names in the about page
    test('about page', () => {
      render(<About></About>);
      const abtElement = screen.getByTestId("about title"); 
      expect(abtElement).toBeInTheDocument();

      //verify names in about page
      const names = ["Adrian Gallardo", "Sidharth Srinivas", "Matthew Nguyen", "Yuvraj Sahu", "Jose Monroy-Soto"];
      names.forEach((item) => {
        const el = screen.getByText(item);
        expect(el).toBeInTheDocument();
      })
    })

    //Test Case 4: checks for integer expression in total commits
    test('about page commit count', () => {
      render(<About></About>);
      const countElement = screen.getByTestId("commit count");
      expect(countElement).toBeInTheDocument();

      const commitCountText = countElement.textContent.replace('Total Commits: ', '').trim();
      const commitCount = parseInt(commitCountText, 10);
      // Assert that the commit count is greater than or equal to 0
      expect(commitCount).toBeGreaterThan(-1);
    })

    //Test Case 5: check for hospital title and hospital page
    test('hospitals page existence', () => {
      render(<Hospitals></Hospitals>);

      const hospitalElement = screen.getByTestId("hospital title");
      expect(hospitalElement).toBeInTheDocument();
      expect(hospitalElement).toHaveTextContent("Hospitals");

      //should be 374 hospitals
      // const hospitalCount = 374;
      // const hospitalElement = screen.getByTestId("hospital count"); 
      // const totalItemsText = hospitalElement.textContent;
      // const regex = /Showing \d+ - \d+ of (\d+) results/;
      // const match = totalItemsText.match(regex);
      // console.log("the total items is: ",totalItemsText)
      // const totalItems = match ? parseInt(match[1], 10) : NaN;
      // expect(totalItems).toBe(hospitalCount);
      
    });

    //Test Case 6: check for hospital instances coutn label
    test('hospitals count existence', () => {
      render(<Hospitals></Hospitals>);

      const hospitalElement = screen.getByTestId("hospital count");
      expect(hospitalElement).toBeInTheDocument();
      expect(hospitalElement).toHaveTextContent("Showing");

      
    });
    
    //Test Case 7: events check for title and page existence
    test('events page existence', () => {
      render(<Events></Events>);

      const eventElement = screen.getByTestId("event title");
      expect(eventElement).toBeInTheDocument();
      expect(eventElement).toHaveTextContent("Events");
    });

    //Test Case 8: events check for count of instances label
    test('events count existence', () => {
      render(<Events></Events>);

      const eventElement = screen.getByTestId("event count");
      expect(eventElement).toBeInTheDocument();
      expect(eventElement).toHaveTextContent("Showing");
    });

    //Test Case 9: check for elderly homes page and title
    test('elderly homes page existence', () => {
      render(<ElderlyHomes></ElderlyHomes>)

      const elderlyElement = screen.getByTestId("home title");
      expect(elderlyElement).toBeInTheDocument();
      expect(elderlyElement).toHaveTextContent("Elder Homes");
    });

    //Test Case 10: events check for count of instances label
    test('elderly homes count existence', () => {
      render(<ElderlyHomes></ElderlyHomes>)

      const elderlyElement = screen.getByTestId("home count");
      expect(elderlyElement).toBeInTheDocument();
      expect(elderlyElement).toHaveTextContent("Showing");
    });
  });