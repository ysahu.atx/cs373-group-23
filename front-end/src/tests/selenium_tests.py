import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


url = "https://www.elderhelpertexas.me/"

class selenium_tests(unittest.TestCase):
    def setUp(self):
        # Set the path to your ChromeDriver executable
        options = Options()        
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument("window-size=2560x1440")
        self.driver = webdriver.Remote(command_executor='http://selenium__standalone-chrome:4444/wd/hub', options=options)
        self.driver.implicitly_wait(10)
        self.driver.get(url)
        return super().setUp()

    def tearDown(self):
        # Close the browser
        self.driver.quit()
        return super().tearDown()

    def test_homepage_title(self):
        # Navigate to the website
        self.driver.get(url)

        # Example assertion: check if the title contains a specific keyword
        self.assertIn("ElderHelperTexas", self.driver.title)
    
    def test_about_content1(self):
        # Navigate to the website
        self.driver.get(url + "about")
        element = self.driver.find_element(By.XPATH, "//h1[contains(text(), 'About Us')]")

        expected = "About Us"
        # Example assertion: check if the title contains a specific keyword
        self.assertEqual(element.text, expected)

    def test_about_content2(self):
        # Navigate to the website
        self.driver.get(url + "about")
        element = self.driver.find_element(By.XPATH, "//h1[contains(text(), 'Total Stats')]")

        expected = "Total Stats"
        # Example assertion: check if the title contains a specific keyword
        self.assertEqual(element.text, expected)
        
    def test_about_content3(self):
        # Navigate to the website
        self.driver.get(url + "about")
        element = self.driver.find_element(By.XPATH, "//h1[contains(text(), 'Tools Used')]")

        expected = "Tools Used"
        # Example assertion: check if the title contains a specific keyword
        self.assertEqual(element.text, expected)
    
    def test_navbar_events(self):
        self.driver.get(url)
        nav_edu = self.driver.find_element(By.XPATH, "//a[@href='/Events']")
        expected = "Events"
        self.assertEqual(nav_edu.text, expected)

        nav_edu.click()
        WebDriverWait(self.driver, 10)
        self.assertEqual(self.driver.current_url, url + "Events")
    
    def test_navbar_about(self):
        self.driver.get(url)
        nav_edu = self.driver.find_element(By.XPATH, "//a[@href='/about']")
        expected = "About"
        self.assertEqual(nav_edu.text, expected)

        nav_edu.click()
        WebDriverWait(self.driver, 10)
        self.assertEqual(self.driver.current_url, url + "about")

    def test_navbar_hospitals(self):
        self.driver.get(url)
        nav_edu = self.driver.find_element(By.XPATH, "//a[@href='/Hospitals']")
        expected = "Hospitals"
        self.assertEqual(nav_edu.text, expected)

        nav_edu.click()
        WebDriverWait(self.driver, 10)
        self.assertEqual(self.driver.current_url, url + "Hospitals")
    
    def test_navbar_ElderlyHomes(self):
        self.driver.get(url)
        nav_edu = self.driver.find_element(By.XPATH, "//a[@href='/ElderlyHomes']")
        expected = "Elderly Homes"
        self.assertEqual(nav_edu.text, expected)

        nav_edu.click()
        WebDriverWait(self.driver, 10)
        self.assertEqual(self.driver.current_url, url + "ElderlyHomes")

    def test_navbar_home(self):
        self.driver.get(url)
        nav_edu = self.driver.find_element(By.XPATH, "//a[@href='/']")
        expected = "ElderHelperTexas"
        self.assertEqual(nav_edu.text, expected)
    
    def test_navbar_logo(self):
        self.driver.get(url)
        nav_edu = self.driver.find_element(By.XPATH, "//span[contains(text(), 'ElderHelperTexas')]")
        expected = "ElderHelperTexas"
        self.assertEqual(nav_edu.text, expected)

if __name__ == "__main__":
    unittest.main()