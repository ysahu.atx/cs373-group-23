SHELL := bash

# Creating python virtual environment
venv:
	cd backend; \
	python3 -m venv venv
	
# Installing dependencies
backend-dep:
	cd backend; \
	source venv/bin/activate; \
	pip install -r requirements.txt

# Running the Flask app
flask:
	cd backend; \
	source venv/bin/activate; \
	python3 app/run.py

run:
	cd ./front-end && npm start


add:
	git add -A
	git status

pull:
	git pull
	git status

install:
	cd ./front-end/src && npm install
