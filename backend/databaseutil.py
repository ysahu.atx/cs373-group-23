from sqlalchemy import create_engine
from backend.config import Settings


def get_engine(): 
    settings = Settings()
    connection_string = f'mysql+mysqlconnector://{settings.database_username}:{settings.database_password}@{settings.database_hostname}:{settings.database_port}'
    engine = create_engine(connection_string)
    return engine