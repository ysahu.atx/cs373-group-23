from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    database_username: str
    database_password: str
    database_hostname: str
    database_port: str
    database_name: str
    google_api: str
    google_crx: str
    ec_token: str
    # google_api_key: str
    # google_cx: str
    # frontend_url: str

    class Config:
        env_file = '.env'


settings = Settings()
