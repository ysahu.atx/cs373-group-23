import requests
from config import Settings
from google_images_search import GoogleImagesSearch
from databaseutil import get_engine
from classes import Homes
from sqlalchemy.orm import sessionmaker

settings = Settings()
gis = GoogleImagesSearch(settings.google_api, settings.google_crx)

def get_image_url(display_name):
    _search_params = {
    'q': display_name,
    'fileType': 'png|jpg',
    'imgType': 'photo' ##
    }
    gis.search(search_params=_search_params)
    for image in gis.results():
        return image.url  # image direct url

def get_json_response(query):
    url = 'https://places.googleapis.com/v1/places:searchText' # Google maps API

    headers = {
        'Content-Type': 'application/json',
        'X-Goog-Api-Key': settings.google_api,
        'X-Goog-FieldMask': 'places.id,places.displayName,places.formattedAddress,places.rating,places.nationalPhoneNumber,places.types,places.websiteUri,places.photos',
    }

    data = {
        'textQuery': query

    }
    return requests.post(url, json=data, headers=headers).json()

def parse_response(response, engine):
# Assuming 'result' is the key containing the place information in the response
    if 'places' in response:
        for place_info in response['places']:
            item_list = [None, None, None, None, None, None, None]
            item_list[0] = place_info.get('id') # place_id
            item_list[1] = place_info.get('displayName')['text'] #display_name
            item_list[2] = place_info.get('nationalPhoneNumber') #national_phone_number
            item_list[3] = place_info.get('formattedAddress') #formatted_address
            item_list[4] = place_info.get('websiteUri') #website_url
            item_list[5] = place_info.get('rating') #rating
            item_list[6] = get_image_url(item_list[1]) #img url
            if place_info.get('websiteUri') and item_list[6]: # Weed out some missing data fields
                add_list_to_database(item_list, engine)

            # print(f"Place ID: {place_id}")
            # print(f"Display Name: {display_name}")
            # print(f"National Phone Number: {national_phone_number}")
            # print(f"Formatted Address: {formatted_address}")
            # print(f"Website URL: {website_url}")
            # print(f"Rating: {rating}")
            # print(f"Photo Name: {photo_name}")
            # print("---")
    else:
        print("No places found in the response.")

def add_list_to_database(item_list, engine):
    Session = sessionmaker(bind=engine)
    session = Session()
    c1 = Homes(place_id=item_list[0], display_name=item_list[1], national_phone_number=item_list[2],
                formatted_address=item_list[3], website_url=item_list[4], rating=item_list[5], image_url=item_list[6])
    session.add(c1)
    session.commit()
    session.close()

def main():
    list_of_cities = ["houston", "san-antonio", "dallas", "austin", "fort-worth", "el-paso", "arlington", 
                      "corpus-christi", "plano", "lubbock", "laredo", "irving", "garland", "frisco", "amarillo", "grand-prairie", 
                      "mckinney", "brownsville", "killeen", "mcallen", "pasadena", "mesquite-city", "denton", "waco", 
                      "midland", "carrollton", "abilene"]
    for city in list_of_cities:
        query = "Nursing Homes in " + city + ", Texas"
        engine = get_engine()
        response = get_json_response(query)
        parse_response(response, engine)

if __name__ == '__main__':
    main()
