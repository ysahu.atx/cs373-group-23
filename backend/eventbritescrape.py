from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from databaseutil import get_engine
from classes import Events
from sqlalchemy.orm import sessionmaker

def get_soup_and_driver(URL):
    driver = webdriver.Chrome()
    driver.get(URL)
    return BeautifulSoup(driver.page_source, 'html.parser'), driver


def parse_web_data(soup, driver, engine):
    class_boxes = soup.findAll("li")
    for card in class_boxes:
        card_name = card.find('a', class_='event-card-link')

        if card_name and 'aria-label' in card_name.attrs and 'data-event-category' in card_name.attrs:
            card_details = card.find_all('p', class_='Typography_root__487rx #585163 Typography_body-md__487rx event-card__clamp-line--one Typography_align-match-parent__487rx')
            image_src = card.find('img', class_='event-card-image')
            real_title = card.find('h2', class_='Typography_root__487rx #3a3247 Typography_body-lg__487rx event-card__clamp-line--two Typography_align-match-parent__487rx')
            item_list = [None, None, None, None, None, None, None]
            item_list[0] = real_title.get_text() # Title
            item_list[1] = card_name['data-event-location'] # City, State
            item_list[2] = card_name['data-event-paid-status'] # Free or paid
            item_list[3] = card_name['data-event-category'] # Misc
            item_list[4] = card_details[0].get_text() # location
            item_list[5] = card_details[1].get_text() # Time

            if image_src:
                item_list[6] = image_src['src'] # image url
                print(item_list)
                add_list_to_database(item_list, engine)

            
def add_list_to_database(item_list, engine):
    Session = sessionmaker(bind=engine)
    session = Session()
    c1 = Events(event_title=item_list[0], event_citystate=item_list[1], event_paid_status=item_list[2],
                event_category=item_list[3], event_location=item_list[4], event_time=item_list[5], event_img_src=item_list[6])
    print(c1)
    #session.add(c1)
    #session.commit()
    session.close()


def main():
    list_of_cities = ["houston", "san-antonio", "dallas", "austin", "fort-worth", "el-paso", "arlington", 
                      "corpus-christi", "plano", "lubbock", "laredo", "irving", "garland", "frisco", "amarillo", "grand-prairie", 
                      "mckinney", "brownsville", "killeen", "mcallen", "pasadena", "mesquite-city", "denton", "waco", 
                      "midland", "carrollton", "abilene"]

    engine = get_engine()
    first = "https://www.eventbrite.com/d/tx--"
    last = "/senior-events/"

    for city in list_of_cities:
        url = first + city + last
        soup, driver = get_soup_and_driver(url)
        parse_web_data(soup, driver, engine)

    driver.quit()

if __name__ == '__main__':
    main()
