from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import String, Integer, Column, Float
Base = declarative_base()

class Hospitals(Base):
    __tablename__ = 'hospital_info'
    __table_args__ = {"schema": "cs373db", "mysql_engine": "InnoDB"}

    idhospital_info = Column(Integer, primary_key=True)
    hospital_name = Column(String)
    city = Column(String)
    beds = Column(String)
    gross_patient_revenue = Column(String)
    patient_days = Column(String)
    total_discharges = Column(String)
    hospital_image_url = Column(String)

class Events(Base):
    __tablename__ = 'event_info'
    __table_args__ = {"schema": "cs373db", "mysql_engine": "InnoDB"}

    idevent_info = Column(Integer, primary_key=True)
    event_title = Column(String)
    event_citystate = Column(String)
    event_paid_status = Column(String)
    event_category = Column(String)
    event_location = Column(String)
    event_time = Column(String)
    event_img_src = Column(String)

class Homes(Base):
    __tablename__ = 'elder_home_info'
    __table_args__ = {"schema": "cs373db", "mysql_engine": "InnoDB"}

    idelder_home_info = Column(Integer, primary_key=True)
    place_id = Column(String)
    display_name = Column(String)
    national_phone_number = Column(String)
    formatted_address = Column(String)
    website_url = Column(String)
    rating = Column(Float)
    image_url = Column(String)