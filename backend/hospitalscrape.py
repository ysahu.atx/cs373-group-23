from bs4 import BeautifulSoup
from selenium import webdriver
from seleniumwire import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
import random
from databaseutil import get_engine
from sqlalchemy.orm import sessionmaker
from classes import Hospitals
from config import Settings

from google_images_search import GoogleImagesSearch


settings = Settings()
gis = GoogleImagesSearch(settings.google_api, settings.google_crx)

def get_hospital_image_url(hospital_name):
    _search_params = {
    'q': hospital_name,
    'fileType': 'png|jpg',
    'imgType': 'photo' ##
    }
    gis.search(search_params=_search_params)
    for image in gis.results():
        return image.url  # image direct url

def get_soup_and_driver():
    # the list of proxy to rotate on 
    PROXIES = [
        'http://38.62.223.210:3128',
        'http://154.6.98.91:3128',
        'http://38.62.223.47:3128',
        'http://154.6.96.217:3128',
    ]

    # randomly extract a proxy
    random_proxy = random.choice(PROXIES)

    # set the proxy in Selenium Wire
    seleniumwire_options = {
        'proxy': {
            'http': f'{random_proxy}',
            'https': f'{random_proxy}',
            'verify_ssl': False,
        },
    }

    # create a ChromeDriver instance
    driver = webdriver.Chrome(
        service=ChromeService(ChromeDriverManager().install()),
        seleniumwire_options=seleniumwire_options
    )
    driver.get("https://www.ahd.com/states/hospital_TX.html")

    return BeautifulSoup(driver.page_source, 'html.parser'), driver

def parse_web_data(soup, driver, engine):
    class_boxes = soup.findAll("tr")
    for hospital in class_boxes:
        hospital_name = hospital.find("a") # Gets each a element with hospital name within
        item_list = [None, None, None, None, None, None, None]
        if hospital_name:
            str_hospital_name = hospital_name.get_text() # Gets hospital name string
            # print(hospital_name)
            item_list[0] = str_hospital_name
            # print(hospital_name.get_text())
            # print(type(hospital_name))
            index = 1
            rest_elements = hospital.findAll("td")
            for element in rest_elements:
                item_list[index] =element.get_text()
                # print(element.get_text())
                index += 1
            if(item_list[0] != None):
                # add to DB
                item_list[6] = get_hospital_image_url(str_hospital_name)
                add_list_to_database(item_list, engine)
        
        # print(rest_elements)

def add_list_to_database(item_list, engine):
    Session = sessionmaker(bind=engine)
    session = Session()
    c1 = Hospitals(hospital_name=item_list[0], city=item_list[1], beds=item_list[2],
                gross_patient_revenue=item_list[3], patient_days=item_list[4], total_discharges=item_list[5], hospital_image_url=item_list[6])
    session.add(c1)
    session.commit()
    session.close()

        

def main():
    
    engine = get_engine()
    soup, driver = get_soup_and_driver()
    parse_web_data(soup, driver, engine)
    driver.quit()

if __name__ == '__main__':
    main()
