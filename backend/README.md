## Backend Structure
- api/ all endpoints to the website and CRUD operations
- models/ representing tables in our database, we are using SQLAlchemy
- services/ handling business logic and external api calls from our data sources

## Local Setup

## Adrian's Local Setup Docker
```
1. In the backend folder "docker build -t python-imagename ."
2. In the backend folder "docker run -p 5000:5000 python-imagename"
3. Server should be up and running.
```

## Adrian's Local Setup Run Without Docker
```
1. In the app folder "python run.py"
```

1. **Update and Install pip, python3.11, and mysqlclient**
```
apt-get update -y
apt-get install -y python3-pip python3.11-dev build-essential
apt-get install -y libmysqlclient-dev
```

2. **Create Virtual Environment**
```
python3 -m venv venv
source venv/bin/activate
```
(Windows):
```
./backend//venv/Scripts/activate
```
Execution Policy:
```
Get-ExecutionPolicy (checks the current execution policy)
Set-ExecutionPolicy RemoteSigned
```

3. **Install Dependencies**
```
pip install -r requirements.txt
```

4. **Set Environment Variables**
```
export FLASK_APP=app.py
export FLASK_ENV=development
```
Create a `.env` file in the backend directory and add the variables in `config.py`.

5. **Run Flask**
```
flask run
```
Make sure to append api to the endpoint to access the api, for example: http://0.0.0.0:5000/api/

## Docker Setup (Optional)
1. **Build Docker Image**
```
docker build -t backend .
```
2. **Run Docker Container**
```
docker run -p 5000:5000 backend
```