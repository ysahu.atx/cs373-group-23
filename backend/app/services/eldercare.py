from zeep import Client
import logging


class ElderCareService:
    def __init__(self, wsdl_url, token):
        self.wsdl_url = wsdl_url
        self.token = token
        self.client = Client(wsdl=wsdl_url)

    def search_by_city_state(self, city, state):
        try:
            response = self.client.service.SearchByCityState(
                asCity=city, asState=state, asToken=self.token)
            return response
        except Exception as e:
            logging.error(
                f"Error calling ElderCareService.SearchByCityState: {e}")
            raise
