from flask import Blueprint, jsonify, request
from models import hospital_info, event_info, elder_home_info
from sqlalchemy import func
from sqlalchemy import or_
from sqlalchemy.sql.sqltypes import Integer
from sqlalchemy.exc import NoResultFound


hospital_blueprint = Blueprint('hospitals', __name__)


@hospital_blueprint.route('/', methods=['GET'])
def get_all_hospitals():
    # Get the search term from the query parameters
    search_term = request.args.get('search', '')
    # Get the sort order from the query parameters (default to ascending)
    sort_order = request.args.get('sort_order', 'asc')

    # Assuming hospital_info is your SQLAlchemy model
    if search_term:
        query = hospital_info.query.filter(
            or_(
                hospital_info.hospital_name.ilike(f"%{search_term}%"),
                # Add more columns as needed
                hospital_info.city.ilike(f"%{search_term}%"),
                hospital_info.beds.ilike(f"%{search_term}%"),
                hospital_info.gross_patient_revenue.ilike(f"%{search_term}%"),
                hospital_info.patient_days.ilike(f"%{search_term}%"),
                hospital_info.total_discharges.ilike(f"%{search_term}%"),
                # Add more columns as needed
            )
        )
        # print(type(query))
    else:
        query = hospital_info.query
        # print(type(query))
        # if sort_order == 'hospitalnameasc':
        #     results.sort(key=lambda x: x.hospital_name, reverse=False)
        # elif sort_order == 'hospitalnamedesc':
        #     results.sort(key=lambda x: x.hospital_name, reverse=True)
        # elif sort_order == 'cityasc':
        #     results.sort(key=lambda x: x.city, reverse=False)
        # elif sort_order == 'citydesc':
        #     results.sort(key=lambda x: x.city, reverse=True)
        # elif sort_order == 'bedsasc':
        #     results.sort(key=lambda x: int(x.beds.replace(',', '')), reverse=False)
        # elif sort_order == 'bedsdesc':
        #     results.sort(key=lambda x: x.beds, reverse=True)
        # elif sort_order == 'grosspatientrevenueasc':
        #     results.sort(key=lambda x: x.gross_patient_revenue, reverse=False)
        # elif sort_order == 'grosspatientrevenuedesc':
        #     results.sort(key=lambda x: x.gross_patient_revenue, reverse=True)
        # elif sort_order == 'patientdaysasc':
        #     results.sort(key=lambda x: x.patient_days, reverse=False)
        # elif sort_order == 'patientdaysdesc':
        #     results.sort(key=lambda x: x.patient_days, reverse=True)
        # elif sort_order == 'totaldischargesasc':
        #     results.sort(key=lambda x: x.total_discharges, reverse=False)
        # elif sort_order == 'totaldischargesdesc':
        #     results.sort(key=lambda x: x.total_discharges, reverse=True)

        # return jsonify(results)

    if sort_order == 'hospitalnameasc':
        query = query.order_by(hospital_info.hospital_name.asc())
    elif sort_order == 'hospitalnamedesc':
        query = query.order_by(hospital_info.hospital_name.desc())
    elif sort_order == 'cityasc':
        query = query.order_by(hospital_info.city.asc())
    elif sort_order == 'citydesc':
        query = query.order_by(hospital_info.city.desc())
    elif sort_order == 'bedsasc':
        query = query.order_by(func.replace(
            hospital_info.beds, ',', '').cast(Integer).asc())
    elif sort_order == 'bedsdesc':
        query = query.order_by(func.replace(
            hospital_info.beds, ',', '').cast(Integer).desc())
    elif sort_order == 'grosspatientrevenueasc':
        query = query.order_by(func.replace(func.replace(
            hospital_info.gross_patient_revenue, ',', ''), '$', '').cast(Integer).asc())
    elif sort_order == 'grosspatientrevenuedesc':
        query = query.order_by(func.replace(func.replace(
            hospital_info.gross_patient_revenue, ',', ''), '$', '').cast(Integer).desc())
    elif sort_order == 'patientdaysasc':
        query = query.order_by(func.replace(
            hospital_info.patient_days, ',', '').cast(Integer).asc())
    elif sort_order == 'patientdaysdesc':
        query = query.order_by(func.replace(
            hospital_info.patient_days, ',', '').cast(Integer).desc())
    elif sort_order == 'totaldischargesasc':
        query = query.order_by(func.replace(
            hospital_info.total_discharges, ',', '').cast(Integer).asc())
    elif sort_order == 'totaldischargesdesc':
        query = query.order_by(func.replace(
            hospital_info.total_discharges, ',', '').cast(Integer).desc())

    try:
        results = query.distinct().all()
        if not results:
            # Return 404 status code for no results found
            return jsonify({"error": "No results found"}), 404
        return jsonify(results)
    except NoResultFound:
        # Return 404 status code for no results found
        return jsonify({"error": "No results found"}), 404


@hospital_blueprint.route('/<int:idhospital_info>', methods=['GET'])
def get_hospital(idhospital_info):
    hospital = hospital_info.query.get(idhospital_info)
    if not hospital:
        return jsonify({'error': 'Event not found'}), 404

    # Create JSON response
    event_data = {
        'idhospital_info': hospital.idhospital_info,
        'hospital_name': hospital.hospital_name,
        'city': hospital.city,
        'beds': hospital.beds,
        'total_discharges': hospital.total_discharges,
        'patient_days': hospital.patient_days,
        'gross_patient_revenue': hospital.gross_patient_revenue,
        'hospital_image_url': hospital.hospital_image_url,
        'related_events': hospital.related_events,
        'related_homes': hospital.related_homes,
    }

    return jsonify(event_data)


events_blueprint = Blueprint('events', __name__)


@events_blueprint.route('/', methods=['GET'])
def get_all_events():
    # events = event_info.query.all()
    # print(events)
    # return jsonify(events)
    # Get the search term from the query parameters
    search_term = request.args.get('search', '')
    # Get the sort order from the query parameters (default to ascending)
    sort_order = request.args.get('sort_order', 'asc')

    if search_term:
        query = event_info.query.filter(
            or_(
                event_info.event_title.ilike(f"%{search_term}%"),
                # Add more columns as needed
                event_info.event_city.ilike(f"%{search_term}%"),
                event_info.event_paid_status.ilike(f"%{search_term}%"),
                event_info.event_category.ilike(f"%{search_term}%"),
                event_info.event_location.ilike(f"%{search_term}%"),
                event_info.event_time.ilike(f"%{search_term}%"),
                # Add more columns as needed
            )
        )
    else:
        query = event_info.query

    if sort_order == 'eventtitleasc':
        query = query.order_by(event_info.event_title.asc())
    elif sort_order == 'eventtitledesc':
        query = query.order_by(event_info.event_title.desc())
    elif sort_order == 'eventcityasc':
        query = query.order_by(event_info.event_city.asc())
    elif sort_order == 'eventcitydesc':
        query = query.order_by(event_info.event_city.desc())
    elif sort_order == 'eventpaidstatusasc':
        query = query.order_by(event_info.event_paid_status.asc())
    elif sort_order == 'eventpaidstatusdesc':
        query = query.order_by(event_info.event_paid_status.desc())
    elif sort_order == 'eventcategoryasc':
        query = query.order_by(event_info.event_category.asc())
    elif sort_order == 'eventcategorydesc':
        query = query.order_by(event_info.event_category.desc())
    elif sort_order == 'eventlocationasc':
        query = query.order_by(event_info.event_location.asc())
    elif sort_order == 'eventlocationdesc':
        query = query.order_by(event_info.event_location.desc())
    elif sort_order == 'eventtimeasc':
        query = query.order_by(event_info.event_time.asc())
    elif sort_order == 'eventtimedesc':
        query = query.order_by(event_info.event_time.desc())

    try:
        results = query.distinct().all()
        if not results:
            # Return 404 status code for no results found
            return jsonify({"error": "No results found"}), 404
        return jsonify(results)
    except NoResultFound:
        # Return 404 status code for no results found
        return jsonify({"error": "No results found"}), 404


@events_blueprint.route('/<int:idevent_info>', methods=['GET'])
def get_event(idevent_info):
    event = event_info.query.get(idevent_info)
    if not event:
        return jsonify({'error': 'Event not found'}), 404

    # Create JSON response
    event_data = {
        'idevent_info': event.idevent_info,
        'event_title': event.event_title,
        'event_city': event.event_city,
        'event_paid_status': event.event_paid_status,
        'event_category': event.event_category,
        'event_location': event.event_location,
        'event_time': event.event_time,
        'event_img_src': event.event_img_src,
        'related_homes': event.related_homes,
        'related_hospitals': event.related_hospitals
    }

    return jsonify(event_data)


elder_home_blueprint = Blueprint('elderhomes', __name__)


@elder_home_blueprint.route('/', methods=['GET'])
def get_all_events():
    # elder_homes = elder_home_info.query.all()
    # print(elder_homes)
    # return jsonify(elder_homes)
    # Get the search term from the query parameters
    search_term = request.args.get('search', '')
    # Get the sort order from the query parameters (default to ascending)
    sort_order = request.args.get('sort_order', 'asc')

    if search_term:
        query = elder_home_info.query.filter(
            or_(
                elder_home_info.display_name.ilike(f"%{search_term}%"),
                elder_home_info.national_phone_number.ilike(
                    f"%{search_term}%"),  # Add more columns as needed
                elder_home_info.formatted_address.ilike(f"%{search_term}%"),
                elder_home_info.rating.ilike(f"%{search_term}%"),
                elder_home_info.city.ilike(f"%{search_term}%"),
                # Add more columns as needed
            )
        )
    else:
        query = elder_home_info.query

    if sort_order == 'displaynameasc':
        query = query.order_by(elder_home_info.display_name.asc())
    elif sort_order == 'displaynamedesc':
        query = query.order_by(elder_home_info.display_name.desc())
    elif sort_order == 'nationalphonenumberasc':
        query = query.order_by(elder_home_info.national_phone_number.asc())
    elif sort_order == 'nationalphonenumberdesc':
        query = query.order_by(elder_home_info.national_phone_number.desc())
    elif sort_order == 'formattedaddressasc':
        query = query.order_by(elder_home_info.formatted_address.asc())
    elif sort_order == 'formattedaddressdesc':
        query = query.order_by(elder_home_info.formatted_address.desc())
    elif sort_order == 'ratingasc':
        query = query.order_by(elder_home_info.rating.asc())
    elif sort_order == 'ratingdesc':
        query = query.order_by(elder_home_info.rating.desc())
    elif sort_order == 'cityasc':
        query = query.order_by(elder_home_info.city.asc())
    elif sort_order == 'citydesc':
        query = query.order_by(elder_home_info.city.desc())

    try:
        results = query.distinct().all()
        if not results:
            # Return 404 status code for no results found
            return jsonify({"error": "No results found"}), 404
        return jsonify(results)
    except NoResultFound:
        # Return 404 status code for no results found
        return jsonify({"error": "No results found"}), 404


@elder_home_blueprint.route('/<int:idelder_home_info>', methods=['GET'])
def get_elder_home(idelder_home_info):
    elderhome = elder_home_info.query.get(idelder_home_info)
    if not elderhome:
        return jsonify({'error': 'Event not found'}), 404

    # Create JSON response
    event_data = {
        'idelder_home_info': elderhome.idelder_home_info,
        'place_id': elderhome.place_id,
        'display_name': elderhome.display_name,
        'national_phone_number': elderhome.national_phone_number,
        'formatted_address': elderhome.formatted_address,
        'website_url': elderhome.website_url,
        'rating': elderhome.rating,
        'image_url': elderhome.image_url,
        'city': elderhome.city,
        'related_events': elderhome.related_events,
        'related_hospitals': elderhome.related_hospitals
    }

    return jsonify(event_data)


home_blueprint = Blueprint('home', __name__)


@home_blueprint.route('/')
def home():
    return """    <h1>ElderHelperTexas API</h1>
    <h2>Endpoints:</h2>
    <ul>
        <li>
            <h3>Hospitals</h3>
            <ul>
                <li>
                    <code>GET /hospitals</code>
                    <ul>
                        <li>Optional query parameters:</li>
                    </ul>
                </li>
                <li>
                    <code>GET /hospitals/&lt;int:id&gt;</code>
                </li>
            </ul>
        </li>
        <li>
            <h3>Events</h3>
            <ul>
                <li>
                    <code>GET /events</code>
                    <ul>
                        <li>Optional query parameters: </li>
                    </ul>
                </li>
                <li>
                    <code>GET /events/&lt;int:id&gt;</code>
                </li>
            </ul>
        </li>
        <li>
            <h3>Elder Homes</h3>
            <ul>
                <li>
                    <code>GET /elderhomes</code>
                    <ul>
                        <li>Optional query parameters: </li>
                    </ul>
                </li>
                <li>
                    <code>GET /elderhomes/&lt;int:id&gt;</code>
                </li>
            </ul>
        </li>
    </ul>

    """
