from flask import Blueprint, jsonify, request
from backend.app.services.eldercare import ElderCareService
import logging
from datetime import datetime
from backend.config import settings

logger = logging.getLogger(__name__)
api_blueprint = Blueprint('api', __name__)


@api_blueprint.route('/ec-search', methods=['GET'])
def search_eldercare():
    city = request.args.get('city')
    state = request.args.get('state')
    token = settings.ec_token
    logger.info(f"city: {city}; state: {state}; token: {token}")

    WSDL_URL = "https://eldercare.acl.gov/webservices/eldercaredata/ec_search.asmx?WSDL"
    eldercare_service = ElderCareService(WSDL_URL, token)
    try:
        response = eldercare_service.search_by_city_state(city, state)
        logger.info(f"response: {response}")
        return jsonify('success'), 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500


@api_blueprint.route('/', methods=['GET'])
def hello_world():
    return jsonify(message="Hello, World!")
