from dataclasses import dataclass
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

@dataclass
class hospital_info(db.Model):
    idhospital_info:int = db.Column(db.Integer, primary_key=True)
    hospital_image_url:str = db.Column(db.String(500), nullable=False)
    total_discharges:str = db.Column(db.String(45), nullable=False)
    patient_days:str = db.Column(db.String(45), nullable=False)
    gross_patient_revenue:str = db.Column(db.String(45), nullable=False)
    beds:str = db.Column(db.String(45), nullable=False)
    beds:str = db.Column(db.String(45), nullable=False)
    city:str = db.Column(db.String(45), nullable=False)
    hospital_name:str = db.Column(db.String(200), nullable=False)
    related_events = db.Column(db.JSON, nullable=False)
    related_homes = db.Column(db.JSON, nullable=False)

@dataclass
class event_info(db.Model):
    idevent_info:int = db.Column(db.Integer, primary_key=True)
    event_title:str = db.Column(db.String(100), nullable=False)
    event_city:str = db.Column(db.String(45), nullable=False)
    event_paid_status:str = db.Column(db.String(45), nullable=False)
    event_category:str = db.Column(db.String(50), nullable=False)
    event_location:str = db.Column(db.String(150), nullable=False)
    event_time:str = db.Column(db.String(45), nullable=False)
    event_img_src:str = db.Column(db.String(300), nullable=False)
    related_homes = db.Column(db.JSON, nullable=False)
    related_hospitals = db.Column(db.JSON, nullable=False)

@dataclass
class elder_home_info(db.Model):
    idelder_home_info:int = db.Column(db.Integer, primary_key=True)
    place_id:str = db.Column(db.String(45), nullable=False)
    display_name:str = db.Column(db.String(250), nullable=False)
    national_phone_number:str = db.Column(db.String(60), nullable=False)
    formatted_address:str = db.Column(db.String(250), nullable=False)
    website_url:str = db.Column(db.String(350), nullable=False)
    rating:str = db.Column(db.Float, nullable=False)
    image_url:str = db.Column(db.String(500), nullable=False)
    city:str = db.Column(db.String(60), nullable=False)
    related_events = db.Column(db.JSON, nullable=False)
    related_hospitals = db.Column(db.JSON, nullable=False)
