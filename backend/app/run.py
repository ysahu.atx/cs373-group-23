from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from models import db
from flask_cors import CORS
import os

# app = Flask(__name__)
def create_app():
    app = Flask(__name__)
    CORS(app)
    app.config['SQLALCHEMY_DATABASE_URI'] = "mysql://cs373:elderhelper@cs373-project-2.crasaqq0eok6.us-east-2.rds.amazonaws.com:3306/cs373db"
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)

    from routes import events_blueprint, hospital_blueprint, elder_home_blueprint, home_blueprint
    app.register_blueprint(hospital_blueprint, url_prefix='/hospitals')
    app.register_blueprint(events_blueprint, url_prefix='/events')
    app.register_blueprint(elder_home_blueprint, url_prefix='/elderhomes')
    app.register_blueprint(home_blueprint, url_prefix='/')
    
    return app

if __name__ == '__main__':
    create_app().run(debug=True, host='0.0.0.0', port=5000)