from databaseutil import get_engine
from classes import Hospitals, Events, Homes
from sqlalchemy.orm import sessionmaker
from sqlalchemy import select


def add_list_to_database(hospital, item, session):
    hospital.related_events = item
    session.commit()
    print("end")


def main():
    engine = get_engine()
    Session = sessionmaker(bind=engine)
    session = Session()

    # hospitals = session.execute(select(Hospitals))
    # homes = session.execute(select(Homes))
    index = 1541
    while(index != 1915):
        hospital = session.execute(select(Hospitals).where(Hospitals.idhospital_info == index))
        hospital = hospital.scalar()
        index+=1
        print("index is", index)
        empty_list = []
        print(hospital.city)
        events = session.execute(select(Events).where(Events.event_city == hospital.city ))
        for event in events.scalars():
            if(hospital.city == event.event_city):
                empty_list.append(event.idevent_info)
                print(hospital.idhospital_info)
                print(event.idevent_info)
                if(len(empty_list) == 8):
                    break
        if(len(empty_list) > 0):
            add_list_to_database(hospital, empty_list, session)

    session.close

if __name__ == '__main__':
    main()