from databaseutil import get_engine
from classes import Hospitals, Events, Homes
from sqlalchemy.orm import sessionmaker
from sqlalchemy import select
import random


def add_list_to_database(hospital, item, session):
    hospital.related_homes = item
    session.commit()
    print("end")


def main():
    engine = get_engine()
    Session = sessionmaker(bind=engine)
    session = Session()

    # hospitals = session.execute(select(Hospitals))
    # homes = session.execute(select(Homes))
    index = 1541
    while(index != 1915):
        hospital = session.execute(select(Hospitals).where(Hospitals.idhospital_info == index))
        hospital = hospital.scalar()
        index+=1
        print("index is", index)
        empty_list = []
        # print(hospital.city)
        homes = session.execute(select(Homes).where(Homes.city == hospital.city))
        for home in homes.scalars():
            if(hospital.city == home.city):
                empty_list.append(home.idelder_home_info)
                print(hospital.idhospital_info)
                print(home.idelder_home_info)
                if(len(empty_list) == 8):
                    break
        if(len(empty_list) > 0):
            add_list_to_database(hospital, empty_list, session)

    session.close

if __name__ == '__main__':
    main()