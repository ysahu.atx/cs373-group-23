from databaseutil import get_engine
from classes import Hospitals, Events, Homes
from sqlalchemy.orm import sessionmaker
from sqlalchemy import select


def add_list_to_database(event, item, session):
    event.related_hospitals = item
    session.commit()
    print("end")


def main():
    engine = get_engine()
    Session = sessionmaker(bind=engine)
    session = Session()

    # hospitals = session.execute(select(Hospitals))
    # homes = session.execute(select(Homes))
    index = 1
    while(index != 541):
        event = session.execute(select(Events).where(Events.idevent_info == index))
        event = event.scalar()
        index+=1
        print("index is", index)
        empty_list = []
        print(event.event_city)
        hospitals = session.execute(select(Hospitals).where(Hospitals.city ==event.event_city ))
        for hospital in hospitals.scalars():
            if(event.event_city == hospital.city):
                empty_list.append(hospital.idhospital_info)
                print(event.idevent_info)
                print(hospital.idhospital_info)
                if(len(empty_list) == 8):
                    break
        if(len(empty_list) > 0):
            add_list_to_database(event, empty_list, session)

    session.close

if __name__ == '__main__':
    main()