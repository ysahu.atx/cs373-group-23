from databaseutil import get_engine
from classes import Hospitals, Events, Homes
from sqlalchemy.orm import sessionmaker
from sqlalchemy import select
import random


def add_list_to_database(event, item, session):
    event.related_homes = item
    session.commit()
    print("end")


def main():
    engine = get_engine()
    Session = sessionmaker(bind=engine)
    session = Session()

    hospitals = session.execute(select(Hospitals))
    # homes = session.execute(select(Homes))
    index = 1
    while(index != 541):
        event = session.execute(select(Events).where(Events.idevent_info == index))
        event = event.scalar()
        index+=1
        print("index is", index)
        empty_list = []
        print(event.event_city)
        homes = session.execute(select(Homes).where(Homes.city == event.event_city))
        for home in homes.scalars():
            if(event.event_city == home.city):
                empty_list.append(home.idelder_home_info)
                print(event.idevent_info)
                print(home.idelder_home_info)
                if(len(empty_list) == 8):
                    break
        if(len(empty_list) > 0):
            add_list_to_database(event, empty_list, session)

    session.close

if __name__ == '__main__':
    main()